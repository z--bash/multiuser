BROWSER_0="/usr/bin/firefox"
BROWSER_1="/usr/bin/opera-developer --password-store=basic --enable-features=VaapiVideoEncoder,VaapiVideoDecoder,CanvasOopRasterization --use-vulkan"

[ -n "$1" ] && LINK=$1
option=$(\
    zenity \
        --question \
        --title='Browser selection' \
        --text="Which browser to use?\n\nLink: ${LINK:-about://blank}" \
        --cancel-label='-' \
        --extra-button=Opera --ok-label=Firefox \
)
choice_made=$?

if [ "$choice_made" = '0' ]; then
    $BROWSER_0 $LINK
elif [ -n "$option" ]; then
    $BROWSER_1 $LINK
fi
