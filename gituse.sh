#!/bin/sh

NAME="Mikhail Timofeev"
EMAIL_PERSONAL="mikko.timofeev@gmail.com"
EMAIL_WORK="mikhail.timofeev@sympa.com"

REMOTE_URL=$(git remote -v | head -n1 | awk '{print $2}') \
option=$(\
    zenity \
        --question \
        --title='GIT profile selection' \
        --text="Which profile to use?\n\nLink: ${REMOTE_URL:-new repo}" \
        --cancel-label='-' \
        --extra-button=Work --ok-label=Personal \
)

choice_made=$?

if [ "$choice_made" = '0' ]; then
    EMAIL="$EMAIL_PERSONAL"
elif [ -n "$option" ]; then
    EMAIL="$EMAIL_WORK"
else
    echo 'Canceled'
    exit 0
fi

git config user.email "$EMAIL"
git config --get user.email && {
    git config user.name "$NAME"
} || echo 'Not configured'
